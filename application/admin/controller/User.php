<?php

namespace app\admin\controller;
use app\admin\model\Member as MemberModel;

class User extends Admin
{
    public function index($status = null,$office = null,$type = null,$title = null)
    {
        $data = MemberModel::lists($status,$office,$type,$title);
        $this->assign('data_list',$data);
        return $this->fetch();
    }
}