<?php

namespace app\admin\controller;

use think\Controller;
use app\admin\model\Member as MemberModel;
use think\facade\Session;

class Publiz extends Controller
{
    public function login()
    {
        if ($this->request->isPost()){
            $data = [
                'name' => input('username'),
                'password' => input('password'),
            ];
            $result = MemberModel::login($data);
            if ($result){
                $this->redirect('admin/user/index');
            }else{
                $this->error('用户名或密码错误');
            }
        }else{
            return $this->fetch();
        }
    }

    public function logout(){
        if (is_login()){
            Session::clear();
            return $this->success('退出登录成功','login');
        }else{
            return $this->redirect('publiz/logout');
        }
    }
}