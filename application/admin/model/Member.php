<?php

namespace app\admin\model;

use think\db\Where;
use think\Model;

class Member extends Model
{
    protected $table = '__SYS_MEMBER__';
    public $autoWriteTimestamp = true;

    public static function login($data)
    {
        $member = self::alias('a')
            ->where('a.status',1)
            ->field('a.id,a.name,a.title,a.password')
            ->find();
        if ($member){
            if ($member['password']===$data['password']){
                session('user_auth.name',$member['name']);
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public static function lists($status,$office,$type,$title)
    {
        $where = new Where();
        if ($status != null && $status != ''){
            $where['a.status'] = $status;
        }else{
            $where['a.status'] = ['>=',0];
        }

        if ($office != '' && !empty($office)){
            $where['a.office'] = $office;
        }

        if($type != '' && !empty($type)){
            $where['a.type'] = $type;
        }

        if ($title != '' && !empty($title)){
            $where['a.title|a.name'] = ['like','%'.$title.'%'];
        }

        return self::alias('a')
            ->where($where)
            ->field('a.*')
            ->order('a.id desc')
            ->paginate();
    }
}